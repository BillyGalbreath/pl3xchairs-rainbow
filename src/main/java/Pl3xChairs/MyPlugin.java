package Pl3xChairs;

import net.pl3x.pl3xchairs.Chair;
import net.pl3x.pl3xchairs.runnables.StartMetrics;
import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import PluginReference.MC_DirectionNESWUD;
import PluginReference.MC_EventInfo;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private MC_Server server;
	private BaseConfig config;
	private Logger logger;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xChairs";
		info.version = "0.2";
		info.description = "Turn stairs into chairs!";
		return info;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		init();
		Pl3xLibs.getLogger(getPluginInfo()).info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable();
		Pl3xLibs.getLogger(getPluginInfo()).info("Plugin disabled.");
	}

	private void init() {
		getConfig();
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
	}

	private void disable() {
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
		config = null;
		logger = null;
	}

	public void reload() {
		disable();
		init();
	}

	public void debug(String msg) {
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug(msg);
		}
	}

	public MC_Server getServer() {
		return server;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
			config.load();
		}
		return config;
	}

	public Logger getLogger() {
		if (logger == null) {
			logger = new Logger(getPluginInfo());
		}
		return logger;
	}

	@Override
	public void onAttemptPlaceOrInteract(MC_Player player, MC_Location loc, MC_EventInfo ei, MC_DirectionNESWUD dir) {
		if (ei.isCancelled) {
			debug("Event canceled elsewhere!");
			return;
		}
		if (player.isSneaking()) {
			debug("Player is sneaking!");
			return;
		}
		Chair chair = new Chair(player, loc, getConfig().getInteger("max-chair-length", 3));
		if (!chair.isValidBlock()) {
			debug("Not valid chair block!");
			return;
		}
		if (!chair.calculateFacing()) {
			debug("Not facing proper direction!");
			return;
		}
		if (!chair.isCorrectlySized()) {
			debug("Not sized correctly!");
			return;
		}
		if (!chair.hasSolidGround()) {
			debug("No solid ground found!");
			return;
		}
		if (getConfig().getBoolean("sign-sides", false) && !chair.hasSideSigns()) {
			debug("No side signs found!");
			return;
		}
		debug("Player has sat down!");
		chair.playerSit();
		ei.isCancelled = true;
	}
}
