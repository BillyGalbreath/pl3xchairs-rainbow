package net.pl3x.pl3xchairs;

import java.util.Arrays;

import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_Block;
import PluginReference.MC_DirectionNESWUD;
import PluginReference.MC_Entity;
import PluginReference.MC_EntityType;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_PotionEffect;
import PluginReference.MC_PotionEffectType;
import PluginReference.MC_World;

public class Chair {
	private MC_Player player = null;
	private MC_World world = null;
	private MC_Block block = null;
	private MC_Location loc = null;
	private MC_Location facing = null;
	private MC_DirectionNESWUD direction = null;
	private MC_Location signLeft = null;
	private MC_Location signRight = null;
	private int maxLength;

	public Chair(MC_Player player, MC_Location loc, int maxLength) {
		this.player = player;
		this.world = player.getWorld();
		this.block = world.getBlockAt(loc);
		this.loc = loc;
		this.maxLength = maxLength > 100 ? 100 : 3;
	}

	public boolean calculateFacing() {
		facing = new MC_Location(loc);
		switch (getDirection(block)) {
			case WEST:
				facing.yaw = 90;
				direction = MC_DirectionNESWUD.WEST;
				break;
			case EAST:
				facing.yaw = 270;
				direction = MC_DirectionNESWUD.EAST;
				break;
			case NORTH:
				facing.yaw = 180;
				direction = MC_DirectionNESWUD.NORTH;
				break;
			case SOUTH:
				facing.yaw = 0;
				direction = MC_DirectionNESWUD.SOUTH;
				break;
			default:
				return false; // possibly upside down?
		}
		return true;
	}

	public boolean isValidBlock() {
		return isValidBlock(block);
	}

	private boolean isValidBlock(MC_Block block) {
		switch (block.getId()) {
			case 53: // OAK_STAIRS
			case 67: // STONE_STAIRS
			case 108: // BRICK_STAIRS
			case 109: // STONE_BRICK_STAIRS
			case 114: // NETHER_BRICK_STAIRS
			case 128: // SANDSTONE_STAIRS
			case 134: // SPRUCE_STAIRS
			case 135: // BIRCH_STAIRS
			case 136: // JUNGLE_STAIRS
			case 156: // QUARTZ_STAIRS
			case 163: // ACACIA_STAIRS
			case 164: // DARK_OAK_STAIRS
			case 180: // RED_SANDSTONE_STAIRS
				return true;
			default:
				return false; // not a chair!
		}
	}

	public boolean isCorrectlySized() {
		MC_DirectionNESWUD test_direction = null;
		switch (direction) {
			case WEST:
				test_direction = MC_DirectionNESWUD.NORTH;
				break;
			case EAST:
				test_direction = MC_DirectionNESWUD.SOUTH;
				break;
			case NORTH:
				test_direction = MC_DirectionNESWUD.EAST;
				break;
			case SOUTH:
				test_direction = MC_DirectionNESWUD.WEST;
				break;
			default:
				break;
		}
		int counter = testSizeInDirection(loc, test_direction, 1);
		counter = testSizeInDirection(loc, getOppositeDirection(test_direction), counter);
		return counter <= maxLength && counter > 0;
	}

	public boolean hasSideSigns() {
		MC_Location locLeft = null;
		MC_Location locRight = null;
		switch (direction) {
			case WEST:
				locLeft = testSignInDirection(loc, MC_DirectionNESWUD.NORTH);
				locRight = testSignInDirection(loc, MC_DirectionNESWUD.SOUTH);
				break;
			case EAST:
				locLeft = testSignInDirection(loc, MC_DirectionNESWUD.SOUTH);
				locRight = testSignInDirection(loc, MC_DirectionNESWUD.NORTH);
				break;
			case NORTH:
				locLeft = testSignInDirection(loc, MC_DirectionNESWUD.EAST);
				locRight = testSignInDirection(loc, MC_DirectionNESWUD.WEST);
				break;
			case SOUTH:
				locLeft = testSignInDirection(loc, MC_DirectionNESWUD.WEST);
				locRight = testSignInDirection(loc, MC_DirectionNESWUD.EAST);
				break;
			default:
				break;
		}
		if (locLeft == null || locRight == null) {
			return false;
		}
		if (world.getSignAt(locLeft) == null) {
			return false;
		}
		if (world.getSignAt(locRight) == null) {
			return false;
		}
		signLeft = locLeft;
		signRight = locRight;
		return signsFacingCorrectly();
	}

	private boolean signsFacingCorrectly() {
		switch (direction) {
			case WEST:
				return getSignDirection(signLeft).equals(MC_DirectionNESWUD.NORTH) && getSignDirection(signRight).equals(MC_DirectionNESWUD.SOUTH);
			case EAST:
				return getSignDirection(signLeft).equals(MC_DirectionNESWUD.SOUTH) && getSignDirection(signRight).equals(MC_DirectionNESWUD.NORTH);
			case NORTH:
				return getSignDirection(signLeft).equals(MC_DirectionNESWUD.EAST) && getSignDirection(signRight).equals(MC_DirectionNESWUD.WEST);
			case SOUTH:
				return getSignDirection(signLeft).equals(MC_DirectionNESWUD.WEST) && getSignDirection(signRight).equals(MC_DirectionNESWUD.EAST);
			default:
				return false; // possibly upside down?
		}
	}

	public boolean hasSolidGround() {
		if (world.getBlockAt(loc.getLocationAtDirection(MC_DirectionNESWUD.DOWN).getLocationAtDirection(MC_DirectionNESWUD.NORTH)).isSolid()) {
			return true;
		}
		if (world.getBlockAt(loc.getLocationAtDirection(MC_DirectionNESWUD.DOWN).getLocationAtDirection(MC_DirectionNESWUD.EAST)).isSolid()) {
			return true;
		}
		if (world.getBlockAt(loc.getLocationAtDirection(MC_DirectionNESWUD.DOWN).getLocationAtDirection(MC_DirectionNESWUD.NORTH).getLocationAtDirection(MC_DirectionNESWUD.EAST)).isSolid()) {
			return true;
		}
		return false;
	}

	public void playerSit() {
		player.teleport(facing);
		MC_Location arrowLoc = new MC_Location(loc.x + .5, loc.y, loc.z + .5, loc.dimension, 0, 0);
		MC_Entity arrow = world.spawnEntity(MC_EntityType.ARROW, arrowLoc, "Stairs!");
		arrow.setInvisible(true);
		arrow.setPotionEffects(Arrays.asList(new MC_PotionEffect(MC_PotionEffectType.INVISIBILITY, 999999999, 999999999)));
		arrow.setRider(player);
	}

	private int testSizeInDirection(MC_Location thisLoc, MC_DirectionNESWUD nextDirection, int counter) {
		if (counter == -1) {
			return -1;
		}
		MC_Location newLoc = thisLoc.getLocationAtDirection(nextDirection);
		MC_Block block = world.getBlockAt(newLoc);
		if (isValidBlock(block) && getDirection(block).equals(direction)) {
			if (counter > maxLength) {
				return -1;
			}
			return testSizeInDirection(newLoc, nextDirection, counter + 1);
		}
		return counter;
	}

	private MC_Location testSignInDirection(MC_Location thisLoc, MC_DirectionNESWUD nextDirection) {
		MC_Location newLoc = thisLoc.getLocationAtDirection(nextDirection);
		MC_Block block = world.getBlockAt(newLoc);
		if (isValidBlock(block) && getDirection(block).equals(direction)) {
			return testSignInDirection(newLoc, nextDirection);
		}
		if (block.getId() != 68) {
			return null;
		}
		return newLoc;
	}

	private MC_DirectionNESWUD getSignDirection(MC_Location loc) {
		MC_Block block = Pl3xLibs.getWorld(loc.dimension).getBlockAt(loc);
		if (block.getId() != 68) {
			return MC_DirectionNESWUD.DOWN;
		}
		switch (block.getSubtype()) {
			case 4:
				return MC_DirectionNESWUD.WEST;
			case 5:
				return MC_DirectionNESWUD.EAST;
			case 2:
				return MC_DirectionNESWUD.NORTH;
			case 3:
				return MC_DirectionNESWUD.SOUTH;
			default:
				return MC_DirectionNESWUD.DOWN;
		}
	}

	private MC_DirectionNESWUD getDirection(MC_Block block) {
		switch (block.getSubtype()) {
			case 0:
				return MC_DirectionNESWUD.WEST;
			case 1:
				return MC_DirectionNESWUD.EAST;
			case 2:
				return MC_DirectionNESWUD.NORTH;
			case 3:
				return MC_DirectionNESWUD.SOUTH;
			default:
				return MC_DirectionNESWUD.DOWN;
		}
	}

	private MC_DirectionNESWUD getOppositeDirection(MC_DirectionNESWUD direction) {
		switch (direction) {
			case WEST:
				return MC_DirectionNESWUD.EAST;
			case EAST:
				return MC_DirectionNESWUD.WEST;
			case NORTH:
				return MC_DirectionNESWUD.SOUTH;
			case SOUTH:
				return MC_DirectionNESWUD.NORTH;
			default:
				return MC_DirectionNESWUD.DOWN;
		}
	}
}
