package net.pl3x.pl3xchairs.runnables;

import java.io.IOException;

import net.pl3x.pl3xlibs.MetricsLite;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xChairs.MyPlugin;

public class StartMetrics extends Pl3xRunnable {
	private MyPlugin plugin;
	private MetricsLite metrics;

	public StartMetrics(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		try {
			metrics = new MetricsLite(plugin.getPluginInfo());
			metrics.start();
		} catch (IOException ignore) {
		}
	}
}
